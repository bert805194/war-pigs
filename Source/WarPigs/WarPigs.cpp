// Copyright Epic Games, Inc. All Rights Reserved.

#include "WarPigs.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, WarPigs, "WarPigs" );
