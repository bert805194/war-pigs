// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "WarPigsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class WARPIGS_API AWarPigsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
